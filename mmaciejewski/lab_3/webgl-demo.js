//
// Start here
//
function main() {

  const canvas = document.querySelector('#glcanvas');
  const context = canvas.getContext('webgl');

  // If we don't have a GL context, give up now

  if (!context) {
    alert('Unable to initialize WebGL. Your browser or machine may not support it.');
    return;
  }

  // Initialize a shader program; this is where all the lighting
  // for the vertices and so forth is established.
  let shaderCompiler = new ShaderCompiler();
  const shaderProgram = shaderCompiler.initializeShaderProgram(context);

  const informationAboutProgram = Information.getInformationAboutShaderProgram(context, shaderProgram);

  //
  // Initialize the buffers we'll need. For this demo, we just
  // have one object -- a simple three-dimensional cube.
  //
  let cubeBuilder = new CubeBuilder();
  const cubeBuffers = cubeBuilder.buildBuffersForCube(context);

  let url = 'cubetexture.png';
  let textureLoader = new TextureLoader();
  const cubeTexture = textureLoader.loadTexture(context, url);
  let then = 0;

  let draft = new Draftsman();

  // Draw the scene repeatedly
  function render(now) {

    // convert to seconds
    now *= 0.001;
    
    const timeDifference = now - then;
    then = now;

    draft.drawScene(context, informationAboutProgram, cubeBuffers, cubeTexture, timeDifference);

    requestAnimationFrame(render);
  }
  requestAnimationFrame(render);
}

main();





