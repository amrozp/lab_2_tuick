class Draftsman {
    cubeRotation = 0.0;
    //
    // Draw the scene.
    //
    drawScene(context, programInformation, buffers, texture, timeDifference) {

        // Clear to black, fully opaque
        context.clearColor(0.0, 0.0, 0.0, 1.0);

        // Clear everything
        context.clearDepth(1.0);

        // Enable depth testing
        context.enable(context.DEPTH_TEST);

        // Near things obscure far things
        context.depthFunc(context.LEQUAL);

        // Clear the canvas before we start drawing on it.
        context.clear(context.COLOR_BUFFER_BIT | context.DEPTH_BUFFER_BIT);

        // Create a perspective matrix, a special matrix that is
        // used to simulate the distortion of perspective in a camera.
        // Our field of view is 45 degrees, with a width/height
        // ratio that matches the display size of the canvas
        // and we only want to see objects between 0.1 units
        // and 100 units away from the camera.

        const fieldOfView = 45 * Math.PI / 180;   // in radians
        const aspect = context.canvas.clientWidth / context.canvas.clientHeight;
        const zNear = 0.1;
        const zFar = 100.0;
        const projectionMatrix = mat4.create();

        // note: glmatrix.js always has the first argument
        // as the destination to receive the result.
        mat4.perspective(projectionMatrix,
            fieldOfView,
            aspect,
            zNear,
            zFar);

        // Set the drawing position to the "identity" point, which is
        // the center of the scene.
        const modelViewMatrix = mat4.create();

        // Now move the drawing position a bit to where we want to
        // start drawing the square.

        // destination matrix
        // matrix to translate
        // amount to translate
        mat4.translate(modelViewMatrix, modelViewMatrix, [-0.0, 0.0, -6.0]);

        // destination matrix
        // matrix to rotate
        // amount to rotate in radians
        // axis to rotate around (Z)
        mat4.rotate(modelViewMatrix, modelViewMatrix, this.cubeRotation, [0, 0, 1]);

        // destination matrix
        // matrix to rotate
        // amount to rotate in radians
        // axis to rotate around (X)
        mat4.rotate(modelViewMatrix,
            modelViewMatrix,
            this.cubeRotation * .7,
            [0, 1, 0]);

        const normalMatrix = mat4.create();
        mat4.invert(normalMatrix, modelViewMatrix);
        mat4.transpose(normalMatrix, normalMatrix);

        // Tell WebGL how to pull out the positions from the position
        // buffer into the vertexPosition attribute
        {
            const numberOfComponents = 3;
            const type = context.FLOAT;
            const normalize = false;
            const stride = 0;
            const offset = 0;
            context.bindBuffer(context.ARRAY_BUFFER, buffers.position);
            context.vertexAttribPointer(
                programInformation.attribLocations.vertexPosition,
                numberOfComponents,
                type,
                normalize,
                stride,
                offset);
            context.enableVertexAttribArray(
                programInformation.attribLocations.vertexPosition);
        }

        // Tell WebGL how to pull out the texture coordinates from
        // the texture coordinate buffer into the textureCoord attribute.
        {
            const numberOfComponents = 2;
            const type = context.FLOAT;
            const normalize = false;
            const stride = 0;
            const offset = 0;
            context.bindBuffer(context.ARRAY_BUFFER, buffers.textureCoord);
            context.vertexAttribPointer(
                programInformation.attribLocations.textureCoord,
                numberOfComponents,
                type,
                normalize,
                stride,
                offset);
            context.enableVertexAttribArray(
                programInformation.attribLocations.textureCoord);
        }

        // Tell WebGL how to pull out the normals from
        // the normal buffer into the vertexNormal attribute.
        {
            const numberOfComponents = 3;
            const type = context.FLOAT;
            const normalize = false;
            const stride = 0;
            const offset = 0;
            context.bindBuffer(context.ARRAY_BUFFER, buffers.normal);
            context.vertexAttribPointer(
                programInformation.attribLocations.vertexNormal,
                numberOfComponents,
                type,
                normalize,
                stride,
                offset);
            context.enableVertexAttribArray(
                programInformation.attribLocations.vertexNormal);
        }

        // Tell WebGL which indices to use to index the vertices
        context.bindBuffer(context.ELEMENT_ARRAY_BUFFER, buffers.indices);

        // Tell WebGL to use our program when drawing

        context.useProgram(programInformation.program);

        // Set the shader uniforms

        context.uniformMatrix4fv(
            programInformation.uniformLocations.projectionMatrix,
            false,
            projectionMatrix);
        context.uniformMatrix4fv(
            programInformation.uniformLocations.modelViewMatrix,
            false,
            modelViewMatrix);
        context.uniformMatrix4fv(
            programInformation.uniformLocations.normalMatrix,
            false,
            normalMatrix);

        // Specify the texture to map onto the faces.

        // Tell WebGL we want to affect texture unit 0
        context.activeTexture(context.TEXTURE0);

        // Bind the texture to texture unit 0
        context.bindTexture(context.TEXTURE_2D, texture);

        // Tell the shader we bound the texture to texture unit 0
        context.uniform1i(programInformation.uniformLocations.uSampler, 0);

        {
            const vertexCount = 36;
            const type = context.UNSIGNED_SHORT;
            const offset = 0;
            context.drawElements(context.TRIANGLES, vertexCount, type, offset);
        }

        // Update the rotation for the next draw

        this.cubeRotation += timeDifference;
    }
}