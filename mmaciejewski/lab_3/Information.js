class Information{

    static getInformationAboutShaderProgram(context, shaderProgram) {
        const programInformation = {
            program: shaderProgram,
            attribLocations: {
                vertexPosition: context.getAttribLocation(shaderProgram, 'aVertexPosition'),
                vertexNormal: context.getAttribLocation(shaderProgram, 'aVertexNormal'),
                textureCoord: context.getAttribLocation(shaderProgram, 'aTextureCoord'),
            },
            uniformLocations: {
                projectionMatrix: context.getUniformLocation(shaderProgram, 'uProjectionMatrix'),
                modelViewMatrix: context.getUniformLocation(shaderProgram, 'uModelViewMatrix'),
                normalMatrix: context.getUniformLocation(shaderProgram, 'uNormalMatrix'),
                uSampler: context.getUniformLocation(shaderProgram, 'uSampler'),
            },
        };
        return programInformation;
    }
}